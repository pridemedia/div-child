<?php 
/*
Feature Name:   Sidebars
Description:    Any additional child theme sidebars can be registered here
*/
/************* ACTIVE SIDEBARS ********************/

function plus1_child_register_sidebars() {
    // register_sidebar(array(
    //     'id' => 'home-posts',
    //     'name' => __('Home Posts', 'bonestheme'),
    //     'description' => __('The home posts sidebar.', 'bonestheme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));

    // register_sidebar(array(
    //     'id' => 'hero',
    //     'name' => __('Hero', 'plus1_theme'),
    //     'description' => __('The hero sidebar.', 'plus1_theme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));
    
    //register_sidebar(array(
        //'id' => 'features',
        //'name' => __('Features', 'plus1_theme'),
        // 'description' => __('The features sidebar.', 'plus1_theme'),
        //'before_widget' => '<div id="%1$s" class="widget %2$s">',
        //'after_widget' => '</div>',
        //'before_title' => '<h4 class="widgettitle">',
        //'after_title' => '</h4>',
    //));

    // register_sidebar(array(
    //     'id' => 'cta',
    //     'name' => __('Call to Action', 'plus1_theme'),
    //     'description' => __('The Call to Action sidebar.', 'plus1_theme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '',
    //     'after_title' => '',
    // ));
    
    // register_sidebar(array(
    //     'id' => 'footer1',
    //     'name' => __('Footer 1', 'bonestheme'),
    //     'description' => __('Column 1 footer sidebar.', 'bonestheme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));

    // register_sidebar(array(
    //     'id' => 'footer2',
    //     'name' => __('Footer 2', 'bonestheme'),
    //     'description' => __('Column 2 footer sidebar.', 'bonestheme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));

    // register_sidebar(array(
    //     'id' => 'footer3',
    //     'name' => __('Footer 3', 'bonestheme'),
    //     'description' => __('Column 3 footer sidebar.', 'bonestheme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));

    // register_sidebar(array(
    //     'id' => 'footer4',
    //     'name' => __('Footer 4', 'bonestheme'),
    //     'description' => __('Column 4 footer sidebar.', 'bonestheme'),
    //     'before_widget' => '<div id="%1$s" class="widget %2$s">',
    //     'after_widget' => '</div>',
    //     'before_title' => '<h4 class="widgettitle">',
    //     'after_title' => '</h4>',
    // ));  
} 
add_action( 'widgets_init', 'plus1_child_register_sidebars' );
?>